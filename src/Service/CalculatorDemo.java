package Service;

public interface CalculatorDemo {

    long addition(long a, long b);

    long subtraction(long a, long b);

    long multiplication(long a, long b);

    long division(long a, long b);
}
