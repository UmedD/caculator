package Main;

import Realization.CalculatorArabicDigit;
import Realization.CalculatorRomanDigit;
import Realization.Check;
import Service.CalculatorDemo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        CalculatorDemo calculator = new CalculatorArabicDigit();
        CalculatorRomanDigit calculator2 = new CalculatorRomanDigit();
        Check check = new Check();
        String str = null;

        try {
            str = reader.readLine();
            if (check.checkInput(str)) {
                calculator2.checkRomanDigit(str);
            } else {
                ((CalculatorArabicDigit) calculator).checkArabicDigit(str);
            }
        } catch (ArithmeticException e) {
            System.out.println("На ноль делить нельзя");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
