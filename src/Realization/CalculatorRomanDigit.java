package Realization;

import Service.CalculatorDemo;

import java.util.Iterator;
import java.util.Map;

/**
 * Класс для римских сиволом
 */
public class CalculatorRomanDigit extends CalculatorArabicDigit {

    Repository rep = new Repository();
    Map<String, Integer> map = rep.romanMap();

    public void checkRomanDigit(String str) {
        char[] ch = str.toCharArray();
        String firstDigit = "";
        String secondDigit = "";
        int value1 = 0;
        int value2 = 0;
        long result = 0;
        String lastResult = "";

        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == '+' || ch[i] == '-' || ch[i] == '*' || ch[i] == '/') {
                break;
            } else {
                firstDigit += ch[i];
            }
        }

        for (int j = ch.length - 1; j > 0; j--) {
            if (ch[j] == '+' || ch[j] == '-' || ch[j] == '*' || ch[j] == '/') {
                break;
            } else {
                secondDigit += ch[j];
            }
        }

        String reverseDigit = new StringBuffer(secondDigit).reverse().toString();

        for (Map.Entry entry : map.entrySet()) {

            if (entry.getKey().equals(firstDigit)) {
                value1 = (int) entry.getValue();
            }
            if (entry.getKey().equals(reverseDigit)) {
                value2 = (int) entry.getValue();
            }
        }

        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == '+') {
                result = addition(value1, value2);
            }
            if (ch[i] == '-') {
                result = subtraction(value1, value2);
            }
            if (ch[i] == '*') {
                result = multiplication(value1, value2);
            }
            if (ch[i] == '/') {
                result = division(value1, value2);
            }
        }

        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, Integer> pair = iterator.next();
            if (pair.getValue() == result) {
                lastResult = pair.getKey();
            }
        }
        System.out.println(lastResult);
    }
}
