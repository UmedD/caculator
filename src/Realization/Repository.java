package Realization;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс хранилище
 */
public class Repository {
    protected Map<String, Integer> romanMap() {

        Map<String, Integer> map = new HashMap();
        map.put("I", 1);
        map.put("II", 2);
        map.put("III", 3);
        map.put("IV", 4);
        map.put("V", 5);
        map.put("VI", 6);
        map.put("VII", 7);
        map.put("VIII", 8);
        map.put("IX", 9);
        map.put("X", 10);
        return map;
    }
}
