package Realization;

import Service.CalculatorDemo;

import java.lang.*;

/**
 * start 25.11.19
 * Класс для арабских сиволов
 */
public class CalculatorArabicDigit implements CalculatorDemo {

    public void checkArabicDigit(String str) {
        char[] ch = str.toCharArray();

        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == '+') {
                int a = Integer.parseInt(String.valueOf(ch[0]));
                int b = Integer.parseInt(String.valueOf(ch[2]));
                System.out.println(addition(a, b));
            }
            if (ch[i] == '-') {
                int a = Integer.parseInt(String.valueOf(ch[0]));
                int b = Integer.parseInt(String.valueOf(ch[2]));
                System.out.println(subtraction(a, b));
            }
            if (ch[i] == '*') {
                int a = Integer.parseInt(String.valueOf(ch[0]));
                int b = Integer.parseInt(String.valueOf(ch[2]));
                System.out.println(multiplication(a, b));
            }
            if (ch[i] == '/') {
                int a = Integer.parseInt(String.valueOf(ch[0]));
                int b = Integer.parseInt(String.valueOf(ch[2]));
                System.out.println(division(a, b));
            }
        }
    }

    @Override
    public long addition(long a, long b) {
        long c = a + b;
        return c;
    }

    @Override
    public long subtraction(long a, long b) {
        long c = a - b;
        return c;
    }

    @Override
    public long multiplication(long a, long b) {
        long c = a * b;
        return c;
    }

    @Override
    public long division(long a, long b) {
        long c = a / b;
        return c;
    }
}
