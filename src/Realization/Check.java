package Realization;

/**
 * Клас для проверки ввода, если ввел римские сиволы взывается соответсвующий метод
 */
final public class Check {
    public boolean checkInput(String string) {
        char[] ch = string.toCharArray();
        String[] arrStr = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};
        String firstVersion = "";

        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == '+' || ch[i] == '-' || ch[i] == '*' || ch[i] == '/') {
                break;
            } else {
                firstVersion += ch[i];
            }
        }

        for (int i = 0; i < arrStr.length; i++) {
            if (arrStr[i].equals(firstVersion)) {
                return true;
            }
        }
        return false;
    }
}
